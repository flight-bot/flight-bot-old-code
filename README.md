## How to launch the application
Required software: nodejs, npm

```bash
$ npm install yarn -g
$ yarn install
$ yarn build
$ yarn start
```