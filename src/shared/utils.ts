import locales from '../client/locales.json'

export function formatDate(date: number | Date): string {
	date = new Date(date)
	const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate()
	const month =
		date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1

	return `${day}.${month}.${date.getFullYear()}`
}

export function getHours(date: string | Date): string {
	date = new Date(date)
	const hours = date.getHours() > 9 ? date.getHours() : `0${date.getHours()}`
	const minutes =
		date.getMinutes() > 9 ? date.getMinutes() : `0${date.getMinutes()}`
	return `${hours}:${minutes}`
}

export function getDay(date: number | Date, lang: string): string {
	const weeks = [
		{ ru: 'воскресенье', en: 'sunday' },
		{ ru: 'понедельник', en: 'monday' },
		{ ru: 'вторник', en: 'tuesday' },
		{
			ru: 'среда',
			en: 'wednesday',
		},
		{ ru: 'четверг', en: 'thursday' },
		{ ru: 'пятница', en: 'friday' },
		{
			ru: 'суббота',
			en: 'saturday',
		},
	]
	return weeks[new Date(date).getDay()][lang]
}

export function minutesToTime(m: number, lang: string): string {
	const seconds = m * 60
	let result = ''
	const hours = Math.floor(seconds / 60 / 60)
	if (hours > 0) result += `${hours} ${locales[lang].flights.h} `
	const minutes = Math.floor((seconds - hours * 60 * 60) / 60)
	if (minutes > 0) result += `${minutes} ${locales[lang].flights.m}`
	return result
}
