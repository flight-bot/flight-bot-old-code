import { NextPage } from 'next'

type GetObjDifferentKeys<T, U> = Omit<T, keyof U> & Omit<U, keyof T>
type GetObjSameKeys<T, U> = Omit<T | U, keyof GetObjDifferentKeys<T, U>>
export type DeepMergeTwoTypes<T, U> = Partial<GetObjDifferentKeys<T, U>> & {
	[K in keyof GetObjSameKeys<T, U>]: T[K] | U[K]
}

type lang = {
	lang: 'ru' | 'en'
}

export type Page<T = {}> = NextPage<DeepMergeTwoTypes<lang, T>>
