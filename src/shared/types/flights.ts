type passengerType = { $value: string; attributes: { originalCode: string } }

export type FlightsType = {
	ancillaryFeesRequestNeeded: boolean
	carrier: {
		code: string
		name: string
	}
	charter: boolean
	eticket: boolean
	flightFaresSearchDisabled: boolean
	gds: string
	gdsAccount: string
	latinRegistration: boolean
	mtd: boolean
	penaltyInfos: {
		penaltyInfos: {
			passengerType
		}[]
	}
	price: {
		price: { amount: number; elementType: string; passengerType }[]
	}
	segments: {
		segment: {
			airline: { code: string; name: string }
			availableSeats: number
			board: { code: string; name: string }
			bookingClass: string
			cityBegin: { code: string; name: string }
			cityEnd: { code: string; name: string }
			connected: boolean
			countryBegin: { code: string; name: string }
			countryEnd: { code: string; name: string }
			dateBegin: string
			dateEnd: string
			fareInfos: {
				fareInfos: {
					fareBasis: string
					fareDetails: { brandName: string }
					passengerType
					remarksSearchContext: string
				}[]
			}
			flightNumber: string
			locationBegin: { code: string; name: string }
			locationEnd: { code: string; name: string }
			operatingAirline: { code: string; name: string }
			serviceClass: string
			starting: boolean
			techStops: null
			terminalBegin: string
			terminalEnd: string
			travelDuration: number
		}[]
	}
	taxDetails: {
		taxDetails: { amount: number; taxCode: string; passengerType }[]
	}
	timeLimit: string
	token: string
}[]
