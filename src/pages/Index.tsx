import { Header } from '../client/components/Header'
import { SwitchDirection } from '../client/components/SwitchDirection'
import { DirectionInput } from '../client/components/DirectionInput'
import { CalendarInput } from '../client/components/CalendarInput'
import { Dropdown } from '../client/components/Dropdown'
import { Check } from '../client/components/Check'
import { Page } from '../shared/types/language'
import { useEffect, useState } from 'react'
import { useAppDispatch, useAppSelector } from '../client/hooks/redux'
import { selectLang, setLanguage } from '../client/redux/reducers/langSlice'
import locales from '../client/locales.json'
import {
	changeDirect,
	getSearchData,
	SearchState,
	setClass,
} from '../client/redux/reducers/searchSlice'
import { useRouter } from 'next/router'

const Home: Page = ({ lang }) => {
	const dispatch = useAppDispatch()
	const language = useAppSelector(selectLang)
	const data = useAppSelector(getSearchData)
	const [isVerify, setIsVerify] = useState<boolean>(false)
	const router = useRouter()

	useEffect(() => {
		Telegram.WebApp.expand()
		dispatch(setLanguage(lang))
	}, [])

	useEffect(() => {
		dispatch(setClass(locales[language].classes[0]))
	}, [language])

	function checkAndSend() {
		setIsVerify(true)
		if (
			!data.codeFrom ||
			!data.codeTo ||
			!data.date ||
			Object.values(data.who).reduce((acc, curr) => acc + curr, 0) <= 0
		)
			return
		router.push(`/flights`)
	}

	return (
		<section id="home">
			<Header />
			<SwitchDirection />
			<div
				className={`home__wrapper ${
					isVerify && (!data.codeTo || !data.codeFrom)
						? 'home__wrapper__error'
						: ''
				}`}
			>
				<p className="home__title">{locales[language].index.direction}</p>
				<DirectionInput isStart={false} />
				<DirectionInput isStart={true} />
				<p className="home__error">{locales[language].errors.allDirection}</p>
			</div>
			<div
				className={`home__wrapper ${
					isVerify && (!data.date || (!data.isOneDirection && !data.dateBack))
						? 'home__wrapper__error'
						: ''
				}`}
			>
				{data.isOneDirection ? (
					<>
						<p className="home__title">{locales[language].index.date}</p>
						<CalendarInput />
					</>
				) : (
					<div id="home__wrapper__dates">
						<div>
							<p className="home__title">{locales[language].index.dateTo}</p>
							<CalendarInput isBack={false} />
						</div>
						<div>
							<p className="home__title">{locales[language].index.dateBack}</p>
							<CalendarInput isBack={true} />
						</div>
					</div>
				)}
				<p className="home__error">
					{data.isOneDirection
						? locales[language].errors.notDate
						: locales[language].errors.notDates}
				</p>
			</div>
			<div
				className={`home__wrapper ${
					isVerify &&
					!Object.values(data.who).reduce((acc, curr) => acc + curr, 0)
						? 'home__wrapper__error'
						: ''
				}`}
			>
				<div id="home__dropdown">
					<Dropdown
						title={locales[language].index.who}
						values={locales[language].who}
						type="people"
						defaultValue={getPassengers(data.who, language)}
					/>
					<Dropdown
						title={locales[language].index.class}
						type="class"
						values={locales[language].classes}
						defaultValue={data.class || locales[language].classes[0]}
					/>
				</div>
				<p className="home__error">{locales[language].errors.notPeople}</p>
			</div>
			<div className="home__wrapper">
				<Check
					text={locales[language].index.direct}
					defaultValue={data.direct}
					dispatcher={changeDirect}
				/>
			</div>
			<button id="home__button" onClick={checkAndSend}>
				{locales[language].index.find}
			</button>
			<p id={'privacy'}>{locales[language].index.privacy}</p>
		</section>
	)
}

export function getPassengers(
	passengers: SearchState['who'],
	language: string,
): string {
	if (Object.values(passengers).reduce((acc, curr) => acc + curr, 0) <= 0)
		return locales[language].who[0].title
	return Object.entries(passengers)
		.filter(([, value]) => value)
		.map(
			([key, value]) =>
				`${value} ${locales[language].who
					.find(w => w.key == key)
					.title.toLowerCase()}`,
		)
		.join(', ')
}

Home.getInitialProps = (ctx): any => {
	return {
		lang: ctx.query.lang as string,
	}
}

export default Home
