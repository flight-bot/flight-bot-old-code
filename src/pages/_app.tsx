import type { AppProps } from 'next/app'
import { wrapper } from '../client/redux/store'
import { Provider } from 'react-redux'
import '../client/styles/_global.scss'
import Head from 'next/head'
import { PersistGate } from 'redux-persist/integration/react'
import { persistStore } from 'redux-persist'

function MyApp({ Component, ...rest }: AppProps) {
	const { store, props } = wrapper.useWrappedStore(rest)
	const persistor = persistStore(store)

	return (
		<Provider store={store}>
			<PersistGate persistor={persistor} loading={<div>Loading...</div>}>
				<Head>
					<script src="https://telegram.org/js/telegram-web-app.js"></script>
					<title>Бот-Самолёт</title>
				</Head>
				<Component {...props.pageProps} />
			</PersistGate>
		</Provider>
	)
}

export default MyApp
