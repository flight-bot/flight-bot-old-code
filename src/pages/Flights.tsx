import { Page } from '../shared/types/language'
import { useRequest } from '../client/hooks/useRequest'
import { useEffect, useState } from 'react'
import { useAppSelector } from '../client/hooks/redux'
import { getSearchData } from '../client/redux/reducers/searchSlice'
import { Header } from '../client/components/Header'
import { LoadFlights } from '../client/components/LoadFlights'
import { FlightsType } from '../shared/types/flights'
import { Results } from '../client/components/Results'

const Flights: Page = () => {
	const { loaded, request } = useRequest()
	const [flights, setFlights] = useState<FlightsType>([])
	const data = useAppSelector(getSearchData)

	useEffect(() => {
		request('/api/search', 'POST', JSON.stringify(data)).then(res =>
			setFlights(res.flight),
		)
	}, [data])

	return (
		<section id="flights">
			<Header />
			{loaded ? <Results flights={flights} /> : <LoadFlights />}
		</section>
	)
}

export default Flights
