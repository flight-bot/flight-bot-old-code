import { Body, Controller, Get, Post, Query } from '@nestjs/common'
import { SearchService } from './search.service'
import { SearchDto } from './dto/search.dto'

@Controller('/api/search')
export class SearchController {
	constructor(private readonly searchService: SearchService) {}

	@Post()
	async searchFlights(@Body() searchData: SearchDto) {
		return this.searchService.searchFlights(searchData)
	}

	@Post('tariff')
	async getTariff(
		@Body('token') token: string,
		@Body('seats') seats: SearchDto['who'],
	) {
		return this.searchService.getTariff(token, seats)
	}
}
