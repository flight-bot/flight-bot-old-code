import { Inject, Injectable } from '@nestjs/common'
import { Client } from 'soap'
import { SearchDto } from './dto/search.dto'
import { context } from '../../shared/constants'

const classes = { Бизнес: 'BUSINESS', Эконом: 'ECONOMY', Первый: 'FIRST' }

@Injectable()
export class SearchService {
	constructor(@Inject('SOAP_API') private readonly soapClient: Client) {}

	async searchFlights(searchData: SearchDto) {
		const param = {
			context,
			parameters: {
				eticketsOnly: 'true',
				skipConnected: String(searchData.direct),
				route: searchData.isOneDirection
					? {
							segment: {
								date: new Date(searchData.date).toISOString(),
								locationBegin: { code: searchData.codeFrom },
								locationEnd: { code: searchData.codeTo },
							},
					  }
					: {
							$xml: `<segment><date>${new Date(
								searchData.date,
							).toISOString()}</date><locationBegin><code>${
								searchData.codeFrom
							}</code></locationBegin><locationEnd><code>${
								searchData.codeTo
							}</code></locationEnd></segment><segment><date>${new Date(
								searchData.dateBack,
							).toISOString()}</date><locationBegin><code>${
								searchData.codeTo
							}</code></locationBegin><locationEnd><code>${
								searchData.codeFrom
							}</code></locationEnd></segment>`,
					  },
				seats: {
					seatPreferences: Object.entries(searchData.who)
						.filter(([, v]) => v > 0)
						.map(([p, c]) => {
							return {
								count: c.toString(),
								passengerType: p,
							}
						}),
				},
				serviceClass:
					classes[searchData.class] || searchData.class.toUpperCase(),
			},
		}
		const [res, test] = await this.soapClient.searchFlightsAsync(param)
		return res.return.flights || { flight: [] }
	}

	async getTariff(token: string, seats: SearchDto['who']) {
		const param = {
			context,
			parameters: {
				flightToken: token,
				seats: {
					seatPreferences: Object.entries(seats)
						.filter(([, v]) => v > 0)
						.map(([p, c]) => {
							return {
								count: c.toString(),
								passengerType: p,
							}
						}),
				},
			},
		}
		const [res, test] = await this.soapClient.searchFlightFaresAsync(param)
		return res.return.flights.flight
	}
}
