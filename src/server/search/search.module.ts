import { forwardRef, Module } from '@nestjs/common'
import { SearchController } from './search.controller'
import { SearchService } from './search.service'
import { AppModule } from '../app.module'

@Module({
	controllers: [SearchController],
	providers: [SearchService],
	imports: [forwardRef(() => AppModule)],
})
export class SearchModule {}
