export class SearchDto {
	isOneDirection: boolean
	to: string
	from: string
	codeTo?: string
	codeFrom?: string
	date?: number
	who: {
		ADULT: number
		CHILD: number
		INFANT: number
		INFANT_WITH_SITE: number
	}
	class: string
	additional: boolean
	direct: boolean
	dateBack?: number
	id?: number
}
