import { Body, Controller, Post, Res } from '@nestjs/common'
import type { Response } from 'express'

@Controller('/api/language')
export class LanguageController {
	@Post()
	changeLanguage(@Body('lang') lang: string, @Res() res: Response) {
		res.cookie('lang', lang, { expires: new Date(Date.now() + 31536000000) })
		res.json({ success: true })
	}
}
