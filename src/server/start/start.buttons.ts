import { Markup } from 'telegraf';

export function actionButtons() {
    return Markup.keyboard(
        [
            Markup.button.callback('⚡️ Тех. поддержка', 'support'),
            Markup.button.callback('📋 Просмотреть заказы', 'orders'),
            Markup.button.callback('❌ Запросить возврат', 'return'),
            Markup.button.callback('✏️ Ответы на вопросы', 'questions'),
            Markup.button.callback('✅ Заказать доп. услугу', 'services'),
            Markup.button.callback('☎️ Контакты', 'contacts'),
        ],
        {
            columns: 3,
        },
    );
}
