import { InjectBot, Start, Update } from 'nestjs-telegraf'
import { Telegraf } from 'telegraf'
import type { Context } from '../context.interface'
import { actionButtons } from './start.buttons'

@Update()
export class StartUpdate {
	constructor(@InjectBot() private readonly bot: Telegraf<Context>) {}

	@Start()
	async startCommand(ctx: Context) {
		await ctx.reply('Добро пожаловать в Бот-Самолёт', actionButtons())
	}
}
