import { Controller, Get, Logger, Render, Req } from '@nestjs/common'
import { AppService } from './app.service'
import { Lang } from './decorators/lang.decorator'

@Controller()
export class AppController {
	constructor(private readonly appService: AppService) {}

	@Get('/')
	@Render('Index')
	home(@Lang() lang) {
		return {
			lang,
		}
	}

	@Get('/flights')
	@Render('Flights')
	flights(@Lang() lang) {
		return {
			lang,
		}
	}
}
