import { createParamDecorator, ExecutionContext } from '@nestjs/common'
import { Request } from 'express'

export const Lang = createParamDecorator(
	(data: unknown, ctx: ExecutionContext) => {
		const request: Request = ctx.switchToHttp().getRequest()
		return (request.cookies.lang || 'ru') as 'ru' | 'en'
	},
)
