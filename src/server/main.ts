import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { PORT } from '../shared/constants'
import cookieParser from 'cookie-parser'
import { readFileSync } from 'fs'
import { join } from 'path'
import express from 'express'
import cors from 'cors'
import {
	ExpressAdapter,
	NestExpressApplication,
} from '@nestjs/platform-express'
import { createServer } from 'https'

async function bootstrap() {
	const privateKey = readFileSync(join(process.cwd(), 'privatekey.pem'))
	const certificate = readFileSync(join(process.cwd(), 'certificate.pem'))
	const server = express()
	const app = await NestFactory.create<NestExpressApplication>(
		AppModule,
		new ExpressAdapter(server),
	)
	app.use(cookieParser())
	app.use(cors({
		credentials: true,
		origin: true
	}))
	await app.init()
	createServer({ key: privateKey, cert: certificate }, server).listen(PORT)
}

bootstrap()
