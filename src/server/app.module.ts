import { Module } from '@nestjs/common'
import { RenderModule } from 'nest-next'
import Next from 'next'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import LocalSession from 'telegraf-session-local'
import { TelegrafModule } from 'nestjs-telegraf'
import { StartModule } from './start/start.module'
import { LanguageModule } from './language/language.module'
import { SearchModule } from './search/search.module'
import { createClientAsync } from 'soap'
import { SOAP_API_URL } from '../shared/constants'

const sessions = new LocalSession({ database: 'session_db.json' })

const soapFactory = {
	provide: 'SOAP_API',
	useFactory: async () => {
		return await createClientAsync(SOAP_API_URL)
	},
}

@Module({
	imports: [
		RenderModule.forRootAsync(
			Next({ dev: process.env.NODE_ENV == 'development' }),
			{
				viewsDir: null,
				dynamicRoutes: [''],
			},
		),
		TelegrafModule.forRoot({
			middlewares: [sessions.middleware()],
			token: '5573485639:AAGXqERNUMQQZBkHbz8CC1i6wbGKjmFA3LU',
		}),
		StartModule,
		LanguageModule,
		SearchModule,
	],
	controllers: [AppController],
	providers: [AppService, soapFactory],
	exports: ['SOAP_API'],
})
export class AppModule {}
