import { FC, useState } from 'react'
import CalendarSVG from '../assets/calendar.svg'
import { Calendar } from './Calendar'
import { DownModal } from './DownModal'
import { useAppSelector } from '../hooks/redux'
import { selectLang } from '../redux/reducers/langSlice'
import locales from '../locales.json'
import { getDate, getDateBack } from '../redux/reducers/searchSlice'
import { formatDate, getDay } from '../../shared/utils'

export const CalendarInput: FC<{ isBack?: boolean }> = ({ isBack }) => {
	const [isOpen, setIsOpen] = useState<boolean>(false)
	const date = useAppSelector(!isBack ? getDate : getDateBack)
	const lang = useAppSelector(selectLang)

	return (
		<>
			<div id="calendar-input" onClick={() => setIsOpen(prev => !prev)}>
				<div className={!date ? 'calendar-input__error' : ''}>
					{date ? (
						<>
							<span>{formatDate(date)}</span>
							{isBack == undefined ? getDay(date, lang) : ''}
						</>
					) : (
						locales[lang].index.choice_date
					)}
				</div>
				<CalendarSVG />
			</div>
			<DownModal
				title={locales[lang].index.choice_date}
				close={() => setIsOpen(prev => !prev)}
				isOpen={isOpen}
			>
				<Calendar close={() => setIsOpen(prev => !prev)} isBack={isBack} />
			</DownModal>
		</>
	)
}
