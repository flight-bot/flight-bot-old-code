import { FC, useState } from 'react'
import { FlightsType } from '../../shared/types/flights'
import { useAppSelector } from '../hooks/redux'
import { selectLang } from '../redux/reducers/langSlice'
import locales from '../locales.json'
import Way from '../assets/way.svg'
import Airplane from '../assets/airplane_green.svg'
import More from '../assets/more.svg'
import { getIsOneDirection } from '../redux/reducers/searchSlice'
import { getHours, minutesToTime } from '../../shared/utils'
import { DownModal } from './DownModal'
import { AboutFlight } from './AboutFlight'

export const FlightPreview: FC<{ flight: FlightsType[0] }> = ({ flight }) => {
	const lang = useAppSelector(selectLang)
	const isOneDirection = useAppSelector(getIsOneDirection)
	const [isOpen, setIsOpen] = useState<boolean>(false)

	return (
		<>
			<div className="flight-preview">
				<p>{flight.carrier.name}</p>
				<div className="flight-preview__block">
					<div className="flight-preview__block__flex">
						<div className="flight-preview__block__flex__date">
							<p>{getHours(flight.segments.segment[0].dateBegin)}</p>
							<p>{locales[lang].flights.departure}</p>
						</div>
						<div className="flight-preview__block__flex__way">
							<Airplane />
							<Way />
							<p>{`${minutesToTime(
								flight.segments.segment.reduce(
									(acc, val) => acc + val.travelDuration,
									0,
								),
								lang,
							)} ${locales[lang].flights.road}`}</p>
						</div>
						<div
							className="flight-preview__block__flex__date"
							style={{ alignItems: 'flex-end' }}
						>
							<p>
								{getHours(
									flight.segments.segment[flight.segments.segment.length - 1]
										.dateEnd,
								)}
							</p>
							<p>{locales[lang].flights.arrival}</p>
						</div>
					</div>
					<div
						className="flight-preview__block__flex"
						style={{ marginBottom: '22px' }}
					>
						<div className="flight-preview__block__flex__cities">
							<p>
								{flight.segments.segment
									.map((s, i, arr) =>
										i == arr.length && arr.length > 1
											? `${s.cityBegin.name} - ${s.cityEnd.name}`
											: s.cityBegin.name,
									)
									.join(' - ')}
							</p>
							<p>
								{flight.segments.segment.length <= (isOneDirection ? 1 : 2)
									? locales[lang].flights['non-stop']
									: `${
											flight.segments.segment.length - (isOneDirection ? 2 : 3)
									  } ${locales[lang].flights.transfer} + ${minutesToTime(
											flight.segments.segment
												.slice(1, flight.segments.segment.length - 1)
												.reduce((acc, val) => acc + val.travelDuration, 0),
											lang,
									  )} ${locales[lang].flights.road}`}
							</p>
						</div>
						<a onClick={() => setIsOpen(prev => !prev)}>
							{locales[lang].flights.more}
						</a>
					</div>
					<div className="flight-preview__block__flex flight-preview__block__flex__class">
						<p>{flight.segments.segment[0].serviceClass}</p>
						<More />
					</div>
					<div className="flight-preview__block__flex">
						<a>{locales[lang].flights.change}</a>
						<div className="flight-preview__block__flex__prices">
							<p>
								{flight.segments.segment.sort(
									(a, b) => a.availableSeats - b.availableSeats,
								)[0].availableSeats > 10
									? `9+ ${locales[lang].flights.places}`
									: `${
											flight.segments.segment.sort(
												(a, b) => a.availableSeats - b.availableSeats,
											)[0].availableSeats
									  } ${locales[lang].flights.places}`}
							</p>
							<button>
								{flight.price.price.reduce((acc, v) => acc + v.amount, 0)} ₽
							</button>
						</div>
					</div>
				</div>
			</div>
			<DownModal
				title={flight.carrier.name}
				close={() => setIsOpen(prev => !prev)}
				isOpen={isOpen}
			>
				<AboutFlight flight={flight} />
			</DownModal>
		</>
	)
}
