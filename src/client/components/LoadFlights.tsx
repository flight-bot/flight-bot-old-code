import { FC } from 'react'
import Load from '../assets/load.png'
import locales from '../locales.json'
import { useAppSelector } from '../hooks/redux'
import { selectLang } from '../redux/reducers/langSlice'
import { getSearchData } from '../redux/reducers/searchSlice'
import { getPassengers } from '../../pages/Index'
import { formatDate, getDay } from '../../shared/utils'
import Image from 'next/image'

export const LoadFlights: FC = () => {
	const lang = useAppSelector(selectLang)
	const data = useAppSelector(getSearchData)

	return (
		<div id="load-flights">
			<div>
				<Image alt="load" src={Load} width={40} height={40} />
				<p>{locales[lang].flights.looking}</p>
			</div>
			<footer>
				<p>{`${data.from} - ${data.to}`}</p>
				<p>{getPassengers(data.who, lang)}</p>
				<p>
					{data.isOneDirection
						? `${formatDate(data.date)} ${getDay(data.date, lang)}`
						: `${formatDate(data.date)} - ${formatDate(data.dateBack)}`}
				</p>
				<p>{data.class}</p>
			</footer>
		</div>
	)
}
