import { FC } from 'react'
import { useAppDispatch, useAppSelector } from '../hooks/redux'
import { getSearchData } from '../redux/reducers/searchSlice'

export const Check: FC<{
	text: string
	dispatcher: (...args: any) => any
	defaultValue: boolean
}> = ({ text, dispatcher, defaultValue }) => {
	const value = useAppSelector(getSearchData)
	const dispatch = useAppDispatch()

	return (
		<div className="check">
			<input
				type={'checkbox'}
				defaultChecked={defaultValue}
				onClick={() => dispatch(dispatcher())}
			/>
			<p>{text}</p>
		</div>
	)
}
