import { FC, useEffect, useRef, useState } from 'react'
import Airplane from '../assets/airplane.svg'
import { useAppDispatch, useAppSelector } from '../hooks/redux'
import {
	from,
	getSearchData,
	setCodeFrom,
	setCodeTo,
	to,
} from '../redux/reducers/searchSlice'
import { selectLang } from '../redux/reducers/langSlice'
import locales from '../locales.json'
import cities from '../../shared/cities.json'
import { func } from 'prop-types'

type cityType = {
	code: string
	city_ru: string
	city_en: string
	country_code: string
	country?: string
}

export const DirectionInput: FC<{ isStart: boolean }> = ({ isStart }) => {
	const dispatch = useAppDispatch()
	const data = useAppSelector(getSearchData)
	const lang = useAppSelector(selectLang)
	const [selected, setSelected] = useState<null | cityType>(null)
	const [hasFocus, setHasFocus] = useState<boolean>(false)
	const [availableCities, setAvailableCities] = useState<cityType[]>([])
	const ref = useRef<HTMLInputElement>(null)

	function searchAvailableCities(
		value: string = data[isStart ? 'to' : 'from'],
	): typeof cities {
		return cities.filter(city => {
			let name = value.toLowerCase() || null
			return (
				city.city_ru.toLowerCase().startsWith(name) ||
				city.city_en.toLowerCase().startsWith(name)
			)
		})
	}

	useEffect(() => {
		const Cities = searchAvailableCities()
		if (data[isStart ? 'codeTo' : 'codeFrom']) setSelected(Cities[0])
		setAvailableCities(Cities)
	}, [data[isStart ? 'to' : 'from']])

	useEffect(() => {
		function check() {
			if (window.document.activeElement == ref.current) setHasFocus(true)
			else setHasFocus(false)
		}

		document.addEventListener('click', check)
		return () => document.removeEventListener('click', check)
	}, [ref])

	return (
		<div style={{ position: 'relative' }}>
			<div className="direction-input">
				<input
					className={
						(isStart ? data.codeTo : data.codeFrom)
							? ''
							: 'direction-input__error'
					}
					placeholder={
						isStart ? locales[lang].index.from : locales[lang].index.to
					}
					value={isStart ? data.to : data.from}
					ref={ref}
					onChange={e => {
						isStart
							? dispatch(to(e.target.value))
							: dispatch(from(e.target.value))
						dispatch(
							isStart
								? setCodeTo(searchAvailableCities(e.target.value)[0]?.code)
								: setCodeFrom(searchAvailableCities(e.target.value)[0]?.code),
						)
					}}
				/>
				<p
					className={
						(availableCities.length && hasFocus) || selected ? 'show' : 'hide'
					}
				>
					<span>
						{availableCities[0]?.city_ru
							.toLowerCase()
							.startsWith(data[isStart ? 'to' : 'from'].toLowerCase())
							? availableCities[0]?.city_ru
							: availableCities[0]?.city_en}
					</span>
					<span>{availableCities[0]?.country}</span>
					<span>{availableCities[0]?.code}</span>
				</p>
				<Airplane id={isStart ? '' : 'rotate'} />
			</div>
			{Boolean(availableCities.length) && Boolean(hasFocus) && (
				<div id="city__container">
					{availableCities.slice(0, 3).map(availableCity => (
						<div
							key={availableCity.code}
							className={`city ${
								availableCities.length && hasFocus ? 'show' : 'hide'
							}`}
							onClick={() => {
								setSelected(availableCity)
								dispatch(
									isStart
										? setCodeTo(availableCity.code)
										: setCodeFrom(availableCity.code),
								)
								dispatch(
									isStart
										? dispatch(
												to(
													availableCity.city_ru
														.toLowerCase()
														.startsWith(
															data[isStart ? 'to' : 'from'].toLowerCase(),
														)
														? availableCity.city_ru
														: availableCity.city_en,
												),
										  )
										: dispatch(
												from(
													availableCity.city_ru
														.toLowerCase()
														.startsWith(
															data[isStart ? 'to' : 'from'].toLowerCase(),
														)
														? availableCity.city_ru
														: availableCity.city_en,
												),
										  ),
								)
							}}
						>
							<p className="city__name">
								{availableCity.city_ru
									.toLowerCase()
									.startsWith(data[isStart ? 'to' : 'from'].toLowerCase())
									? availableCity.city_ru
									: availableCity.city_en}
							</p>
							<p className="city__country">{availableCity.country}</p>
							<p className="city__code">{availableCity.code}</p>
						</div>
					))}
				</div>
			)}
		</div>
	)
}
