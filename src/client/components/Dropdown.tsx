import { FC, ReactNode, useState } from 'react'
import Drop from '../assets/drop.svg'
import { DownModal } from './DownModal'
import { PeopleSelect } from './PeopleSelect'
import { useAppDispatch } from '../hooks/redux'
import { setClass } from '../redux/reducers/searchSlice'

type props = {
	title: string
	values: any[]
	type: string
	defaultValue: string
}

function switchProps(
	type: string,
	values: props['values'],
	close: () => void,
): ReactNode {
	const dispatch = useAppDispatch()

	switch (type) {
		case 'people':
			return <PeopleSelect values={values} close={close} />
		case 'class':
			return (
				<div id="classes">
					{(values as string[]).map(cl => {
						return (
							<p
								key={cl}
								onClick={() => {
									dispatch(setClass(cl))
									close()
								}}
							>
								{cl}
							</p>
						)
					})}
				</div>
			)
	}
}

export const Dropdown: FC<props> = ({ title, values, defaultValue, type }) => {
	const [isOpen, setIsOpen] = useState<boolean>(false)

	return (
		<>
			<div className="dropdown" id={type == 'people' ? 'dropdown-people' : ''}>
				<p>{title}</p>
				<div onClick={() => setIsOpen(true)}>{defaultValue}</div>
				<Drop />
			</div>
			<DownModal
				title={title}
				close={() => setIsOpen(prev => !prev)}
				isOpen={isOpen}
			>
				{switchProps(type, values, () => setIsOpen(prev => !prev))}
			</DownModal>
		</>
	)
}
