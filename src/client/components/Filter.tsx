import { FC } from 'react'
import { Drop } from './Drop'
import { useAppDispatch, useAppSelector } from '../hooks/redux'
import { selectLang } from '../redux/reducers/langSlice'
import locales from '../locales.json'
import { FlightsType } from '../../shared/types/flights'
import {
	applyFilter,
	getFilter,
	setAirlines,
	setMaxArrival,
	setMaxDeparture,
	setMaxPrice,
	setMinArrival,
	setMinDeparture,
	setMinPrice,
	setRoadMax,
	setRoadMin,
	setTransfer,
	setTransferDurationMax,
	setTransferDurationMin,
} from '../redux/reducers/filterSlice'
import { Range } from './Range'
import { getHours, minutesToTime } from '../../shared/utils'
import { changeDirect, getIsOneDirection } from '../redux/reducers/searchSlice'
import { Check } from './Check'

export const Filter: FC<{ flights: FlightsType; close: () => void }> = ({
	flights,
	close,
}) => {
	const lang = useAppSelector(selectLang)
	const filter = useAppSelector(getFilter)
	const dispatch = useAppDispatch()
	const isOneDirection = useAppSelector(getIsOneDirection)
	const prices = flights.map(flight =>
		flight.price.price.reduce((acc, val) => acc + val.amount, 0),
	)
	const departures = flights.map(flight =>
		Number(getHours(flight.segments.segment[0].dateBegin).replace(':', '.')),
	)
	const arrivals = flights
		.map(flight =>
			Number(
				getHours(flight.segments.segment.slice(-1)[0].dateEnd).replace(
					':',
					'.',
				),
			),
		)
		.flat()
	const transfers = flights
		.map(flight => flight.segments.segment.length - (isOneDirection ? 1 : 2))
		.filter((el, ind, arr) => ind === arr.indexOf(el))
		.sort()
	const transfersDuration = flights
		.map(flight =>
			flight.segments.segment
				.slice(1, flight.segments.segment.length - 1)
				.reduce((acc, val) => acc + val.travelDuration, 0),
		)
		.filter(t => t)
	const roads = flights
		.map(flight =>
			flight.segments.segment.reduce((acc, val) => acc + val.travelDuration, 0),
		)
		.filter(t => t)

	return (
		<div id="filter">
			<Drop
				title={locales[lang].flights.price}
				description={`${
					filter.minPrice && filter.minPrice > Math.min.apply(null, prices)
						? filter.minPrice
						: Math.min.apply(null, prices)
				}-${
					filter.maxPrice && filter.maxPrice < Math.max.apply(null, prices)
						? filter.maxPrice
						: Math.max.apply(null, prices)
				} ₽`}
			>
				<Range
					setFirstDispatch={setMinPrice}
					setSecondDispatch={setMaxPrice}
					parseValue={(arg: number) => `${arg} ₽`}
					first_value={
						filter.minPrice && filter.minPrice > Math.min.apply(null, prices)
							? filter.minPrice
							: Math.min.apply(null, prices)
					}
					second_value={
						filter.maxPrice && filter.maxPrice < Math.max.apply(null, prices)
							? filter.maxPrice
							: Math.max.apply(null, prices)
					}
					min={Math.min.apply(null, prices)}
					max={Math.max.apply(null, prices)}
				/>
			</Drop>
			<Drop
				title={locales[lang].flights.departure}
				description={`${numberToString(
					filter.departureMin &&
						filter.departureMin > Math.min.apply(null, departures)
						? filter.departureMin
						: Math.min.apply(null, departures),
				)}-${numberToString(
					filter.departureMax &&
						filter.departureMax < Math.max.apply(null, departures)
						? filter.departureMax
						: Math.max.apply(null, departures),
				)}`}
			>
				<Range
					setFirstDispatch={setMinDeparture}
					setSecondDispatch={setMaxDeparture}
					parseValue={(arg: number) => numberToString(arg)}
					first_value={
						filter.departureMin &&
						filter.departureMin > Math.min.apply(null, departures)
							? filter.departureMin
							: Math.min.apply(null, departures)
					}
					second_value={
						filter.departureMax &&
						filter.departureMax < Math.max.apply(null, departures)
							? filter.departureMax
							: Math.max.apply(null, departures)
					}
					min={Math.min.apply(null, departures)}
					max={Math.max.apply(null, departures)}
				/>
			</Drop>
			<Drop
				title={locales[lang].flights.arrival}
				description={`${numberToString(
					filter.arrivalMin &&
						filter.arrivalMin > Math.min.apply(null, arrivals)
						? filter.arrivalMin
						: Math.min.apply(null, arrivals),
				)}-${numberToString(
					filter.arrivalMax &&
						filter.arrivalMax < Math.max.apply(null, arrivals)
						? filter.arrivalMax
						: Math.max.apply(null, arrivals),
				)}`}
			>
				<Range
					setFirstDispatch={setMinArrival}
					setSecondDispatch={setMaxArrival}
					parseValue={(arg: number) => numberToString(arg)}
					first_value={
						filter.arrivalMin &&
						filter.arrivalMin > Math.min.apply(null, arrivals)
							? filter.arrivalMin
							: Math.min.apply(null, arrivals)
					}
					second_value={
						filter.arrivalMax &&
						filter.arrivalMax < Math.max.apply(null, arrivals)
							? filter.arrivalMax
							: Math.max.apply(null, arrivals)
					}
					min={Math.min.apply(null, arrivals)}
					max={Math.max.apply(null, arrivals)}
				/>
			</Drop>
			<Drop title={locales[lang].flights.transfers}>
				{transfers.map((transfer, i) => {
					return (
						<Check
							text={
								transfer == 0
									? locales[lang].flights['non-stop']
									: `${transfer} ${locales[lang].flights.transfer}`
							}
							defaultValue={Boolean(filter.transfer?.find(t => t == transfer))}
							dispatcher={setTransfer.bind(null, transfer)}
						/>
					)
				})}
				<Range
					setFirstDispatch={setTransferDurationMin}
					setSecondDispatch={setTransferDurationMax}
					parseValue={(arg: number) => minutesToTime(arg, lang)}
					first_value={
						filter.transferDurationMin &&
						filter.transferDurationMin > Math.min.apply(null, transfersDuration)
							? filter.transferDurationMin
							: Math.min.apply(null, transfersDuration)
					}
					second_value={
						filter.transferDurationMax &&
						filter.transferDurationMax < Math.max.apply(null, transfersDuration)
							? filter.transferDurationMax
							: Math.max.apply(null, transfersDuration)
					}
					min={Math.min.apply(null, transfersDuration)}
					max={Math.max.apply(null, transfersDuration)}
				/>
			</Drop>
			<Drop
				title={locales[lang].flights['road']}
				description={`${minutesToTime(
					filter.roadMin && filter.roadMin > Math.min.apply(null, roads)
						? filter.roadMin
						: Math.min.apply(null, roads),
					lang,
				)} - ${minutesToTime(
					filter.roadMax && filter.roadMax < Math.max.apply(null, roads)
						? filter.roadMax
						: Math.max.apply(null, roads),
					lang,
				)}`}
			>
				<Range
					setFirstDispatch={setRoadMin}
					setSecondDispatch={setRoadMax}
					parseValue={(arg: number) => minutesToTime(arg, lang)}
					first_value={
						filter.roadMin && filter.roadMin > Math.min.apply(null, roads)
							? filter.roadMin
							: Math.min.apply(null, roads)
					}
					second_value={
						filter.roadMax && filter.roadMax < Math.max.apply(null, roads)
							? filter.roadMax
							: Math.max.apply(null, roads)
					}
					min={Math.min.apply(null, roads)}
					max={Math.max.apply(null, roads)}
				/>
			</Drop>
			<Drop title={locales[lang].flights.airlines}>
				{flights
					.map(flight => flight.carrier.name)
					.filter((el, ind, arr) => ind === arr.indexOf(el))
					.map(carrier => (
						<Check
							text={carrier}
							defaultValue={Boolean(filter.airlines?.find(a => a == carrier))}
							dispatcher={setAirlines.bind(null, carrier)}
						/>
					))}
			</Drop>
			<button
				onClick={() => {
					close()
				}}
			>
				{locales[lang].flights.apply}
			</button>
		</div>
	)
}

function numberToString(n: number): string {
	let str = n.toString().replace('.', ':')
	let first = Number(str.split(':')[0])
	let second = str.split(':')[1].toString()
	if (first < 10) str = '0' + str
	if (second.length < 2) str = str + '0'
	if (!second) str = str + ':00'
	return str
}
