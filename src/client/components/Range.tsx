import { FC, useEffect, useState } from 'react'
import { useAppDispatch } from '../hooks/redux'
import { AnyAction } from 'redux'

type props = {
	first_value: number
	second_value: number
	min: number
	max: number
	setFirstDispatch: (...args: any) => any
	setSecondDispatch: (...args: any) => any
	parseValue: (arg: number) => any
}

export const Range: FC<props> = ({
	first_value,
	min,
	second_value,
	max,
	parseValue,
	setSecondDispatch,
	setFirstDispatch,
}) => {
	const [firstValue, setFirstValue] = useState<number>(first_value)
	const [secondValue, setSecondValue] = useState<number>(second_value)
	const [firstIndent, setFirstIndent] = useState<number>(0)
	const [secondIndent, setSecondIndent] = useState<number>(0)
	const dispatch = useAppDispatch()

	useEffect(() => {
		if (firstValue > secondValue) {
			setSecondValue(firstValue)
			setFirstValue(secondValue)
			return
		}
		setFirstIndent(
			prev =>
				Math.floor(((firstValue - min) / (max - min)) * 100) -
				(prev > 50 ? prev / 20 : prev / 40),
		)
		setSecondIndent(
			prev =>
				Math.floor(((secondValue - max) / (max - min)) * 100) * -1 -
				(prev > 50 ? prev / 20 : prev / 15),
		)
	}, [secondValue, firstValue])

	return (
		<div className="range">
			<div className="range__inputs">
				<input
					type="range"
					value={firstValue}
					min={min}
					max={max}
					onChange={e => {
						setFirstValue(Number(e.target.value))
						dispatch(setFirstDispatch(e.target.value))
					}}
				/>
				<input
					type="range"
					value={secondValue}
					min={min}
					max={max}
					onChange={e => {
						setSecondValue(Number(e.target.value))
						dispatch(setSecondDispatch(e.target.value))
					}}
				/>
				<div
					className="range__inputs__left-indent"
					style={{ width: `${firstIndent}%` }}
				></div>
				<div
					className="range__inputs__right-indent"
					style={{ width: `${secondIndent}%` }}
				></div>
			</div>
			<div className="range__values">
				<p>{parseValue(min)}</p>
				<p>{parseValue(max)}</p>
			</div>
		</div>
	)
}
