import { FC } from 'react'
import Logo from '../assets/logo.svg'
import Name from '../assets/name.svg'
import locales from '../locales.json'
import { useAppDispatch, useAppSelector } from '../hooks/redux'
import { changeLanguage, selectLang } from '../redux/reducers/langSlice'

export const Header: FC = () => {
	const lang = useAppSelector(selectLang)
	const dispatch = useAppDispatch()

	return (
		<header>
			<Logo id="logo" />
			<div>
				<Name />
				<p>{locales[lang].header.search}</p>
			</div>
			<p>eng / рус</p>
			<label>
				<input
					type="checkbox"
					defaultChecked={lang === 'ru'}
					onClick={() => {
						dispatch(changeLanguage())
					}}
				/>
				<span />
			</label>
		</header>
	)
}
