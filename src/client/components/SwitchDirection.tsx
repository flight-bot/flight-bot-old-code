import { FC, useState } from 'react'
import { useAppDispatch, useAppSelector } from '../hooks/redux'
import { selectLang } from '../redux/reducers/langSlice'
import locales from '../locales.json'
import {
	getIsOneDirection,
	setIsOneDirection,
} from '../redux/reducers/searchSlice'

export const SwitchDirection: FC = () => {
	const inOneDirection = useAppSelector(getIsOneDirection)
	const lang = useAppSelector(selectLang)
	const dispatch = useAppDispatch()

	return (
		<div id="switch-direction">
			<div
				className={inOneDirection ? 'active' : ''}
				onClick={() => dispatch(setIsOneDirection(true))}
			>
				{locales[lang].direction.one}
			</div>
			<div
				className={inOneDirection ? '' : 'active'}
				onClick={() => dispatch(setIsOneDirection(false))}
			>
				{locales[lang].direction.two}
			</div>
		</div>
	)
}
