import { FC } from 'react'
import { FlightsType } from '../../shared/types/flights'
import Boeing from '../assets/boeing.svg'
import { useAppSelector } from '../hooks/redux'
import { selectLang } from '../redux/reducers/langSlice'
import locales from '../locales.json'
import { getDay, getHours, minutesToTime } from '../../shared/utils'
import { getIsOneDirection } from '../redux/reducers/searchSlice'

export const AboutFlight: FC<{ flight: FlightsType[0] }> = ({ flight }) => {
	const lang = useAppSelector(selectLang)
	const isOneDirection = useAppSelector(getIsOneDirection)
	const date = new Date(flight.segments.segment[0].dateBegin)

	return (
		<div id="about-flight">
			<div id="about-flight__boeing">
				<Boeing />
				<span>{flight.segments.segment.map(s => s.board.name).join(', ')}</span>
			</div>
			<div className="about-flight__container">
				<div id="about-flight__container__cities">
					<p>
						{flight.segments.segment
							.map((s, i, arr) =>
								i == arr.length - 1
									? `${s.cityBegin.name} - ${s.cityEnd.name}`
									: s.cityBegin.name,
							)
							.join(' - ')}
					</p>
					<p>
						{flight.segments.segment.length <= (isOneDirection ? 1 : 2)
							? locales[lang].flights['non-stop']
							: `${
								flight.segments.segment.length - (isOneDirection ? 2 : 3)
							} ${locales[lang].flights.transfer} + ${minutesToTime(
								flight.segments.segment
									.slice(1, flight.segments.segment.length - 1)
									.reduce((acc, val) => acc + val.travelDuration, 0),
								lang,
							)} ${locales[lang].flights.road}`}
					</p>
				</div>
				<div id="about-flight__container__date">
					<p>{`${date.getDate()} ${date.toLocaleString(lang, {
						month: 'long',
					})}`}</p>
					<p>{getDay(date, lang)}</p>
				</div>
			</div>
			{flight.segments.segment.map((segment, i) => (
				<>
					<div
						className="about-flight__container"
						key={`${segment.dateBegin}-${i}-segment`}
					>
						<div className="about-flight__container__column">
							<p>{locales[lang].flights.departure}</p>
							<p>{getHours(segment.dateBegin)}</p>
						</div>
						<div className="about-flight__container__column">
							<p>{locales[lang].flights.airport}</p>
							<p>{segment.locationBegin.name}</p>
						</div>
						<div className="about-flight__container__column">
							<p>{locales[lang].flights.terminal}</p>
							<p>{segment.terminalBegin || segment.terminalEnd}</p>
						</div>
					</div>
					<div className="about-flight__container" key={segment.dateBegin}>
						<div className="about-flight__container__column">
							<p>{locales[lang].flights.arrival}</p>
							<p>{getHours(segment.dateEnd)}</p>
						</div>
						<div className="about-flight__container__column">
							<p>{locales[lang].flights.airport}</p>
							<p>{segment.locationEnd.name}</p>
						</div>
						<div className="about-flight__container__column">
							<p>{locales[lang].flights.terminal}</p>
							<p>{segment.terminalEnd || segment.terminalBegin}</p>
						</div>
					</div>
				</>
			))}
			<div className="about-flight__container">
				<p id="about-flight__container__carrier-text">
					{locales[lang].flights.carrier}
				</p>
			</div>
			<div className="about-flight__container about-flight__container-big">
				<div className="about-flight__container__column">
					<p>{locales[lang].flights.performs}</p>
					<p>{flight.carrier.name}</p>
				</div>
				<div className="about-flight__container__column">
					<p>{locales[lang].flights.flight}</p>
					<p>?</p>
				</div>
			</div>
			<div className="about-flight__container about-flight__container-big">
				<div className="about-flight__container__column">
					<p>{locales[lang].index.class.toLowerCase()}</p>
					<p>{flight.segments.segment[0].serviceClass}</p>
				</div>
				<div className="about-flight__container__column">
					<p>{locales[lang].flights.airplane}</p>
					<p>{flight.segments.segment.map(s => s.board.name).join(', ')}</p>
				</div>
			</div>
			<p>{`${locales[lang].flights.tariff} ?`}</p>
		</div>
	)
}
