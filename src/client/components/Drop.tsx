import { FC, ReactNode, useEffect, useRef, useState } from 'react'
import More from '../assets/more.svg'

type props = {
	title: string
	description?: string
	children?: ReactNode
}

export const Drop: FC<props> = ({ title, children, description }) => {
	const [isOpen, setIsOpen] = useState<boolean>(false)
	const [maxHeight, setMaxHeight] = useState<number>(0)
	const ref = useRef<HTMLDivElement>(null)

	useEffect(() => {
		if (ref.current) setMaxHeight(ref.current.scrollHeight + 50)
	}, [ref])

	return (
		<div className={`drop ${isOpen ? 'drop-open' : 'drop-close'}`}>
			<div className="drop__title" onClick={() => setIsOpen(prev => !prev)}>
				{title}
				<div>
					{description} <More />
				</div>
			</div>
			<div
				className="drop__content"
				ref={ref}
				style={isOpen ? { maxHeight } : {}}
			>
				{children}
			</div>
		</div>
	)
}
