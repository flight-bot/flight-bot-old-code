import { Dispatch, FC, SetStateAction, useEffect, useState } from 'react'
import { FlightsType } from '../../shared/types/flights'
import { useAppDispatch, useAppSelector } from '../hooks/redux'
import { selectLang } from '../redux/reducers/langSlice'
import locales from '../locales.json'
import Link from 'next/link'
import { FlightPreview } from './FlightPreview'
import { DownModal } from './DownModal'
import { Filter } from './Filter'
import {
	applyFilter,
	getApplyFilter,
	getFilter,
} from '../redux/reducers/filterSlice'
import { getHours } from '../../shared/utils'
import { getIsOneDirection } from '../redux/reducers/searchSlice'

const ApplyFilter: FC<{
	setSortedFlights: Dispatch<SetStateAction<FlightsType>>
	flights: FlightsType
}> = ({ flights, setSortedFlights }) => {
	const filter = useAppSelector(getFilter)
	const isOneDirection = useAppSelector(getIsOneDirection)

	useEffect(() => {
		if (filter.applyFilter) {
			let tempSortedFlights: FlightsType = JSON.parse(JSON.stringify(flights))
			if (filter.minPrice)
				tempSortedFlights = tempSortedFlights.filter(
					flight =>
						flight.price.price.reduce((acc, val) => acc + val.amount, 0) >=
						filter.minPrice,
				)
			if (filter.maxPrice)
				tempSortedFlights = tempSortedFlights.filter(
					flight =>
						flight.price.price.reduce((acc, val) => acc + val.amount, 0) <=
						filter.maxPrice,
				)
			if (filter.departureMin)
				tempSortedFlights = tempSortedFlights.filter(
					flight =>
						Number(
							getHours(flight.segments.segment[0].dateBegin).replace(':', '.'),
						) >= filter.departureMin,
				)
			if (filter.departureMax)
				tempSortedFlights = tempSortedFlights.filter(
					flight =>
						Number(
							getHours(flight.segments.segment[0].dateEnd).replace(':', '.'),
						) <= filter.departureMax,
				)
			if (filter.arrivalMin)
				tempSortedFlights = tempSortedFlights.filter(
					flight =>
						Number(
							getHours(flight.segments.segment.slice(-1)[0].dateEnd).replace(
								':',
								'.',
							),
						) >= filter.arrivalMin,
				)
			if (filter.transfer)
				tempSortedFlights = tempSortedFlights.filter(flight =>
					filter.transfer.includes(
						flight.segments.segment.length - (isOneDirection ? 1 : 2),
					),
				)
			if (filter.roadMin)
				tempSortedFlights = tempSortedFlights.filter(
					flight =>
						flight.segments.segment.reduce(
							(acc, val) => acc + val.travelDuration,
							0,
						) >= filter.roadMin,
				)
			if (filter.roadMax)
				tempSortedFlights = tempSortedFlights.filter(
					flight =>
						flight.segments.segment.reduce(
							(acc, val) => acc + val.travelDuration,
							0,
						) <= filter.roadMax,
				)
			if (filter.airlines?.length)
				tempSortedFlights = tempSortedFlights.filter(flight =>
					filter.airlines.includes(flight.carrier.name),
				)
			if (filter.transferDurationMin)
				tempSortedFlights = tempSortedFlights.filter(
					flight =>
						flight.segments.segment
							.slice(1, flight.segments.segment.length - 1)
							.reduce((acc, val) => acc + val.travelDuration, 0) >=
						filter.transferDurationMin,
				)
			if (filter.transferDurationMax)
				tempSortedFlights = tempSortedFlights.filter(
					flight =>
						flight.segments.segment
							.slice(1, flight.segments.segment.length - 1)
							.reduce((acc, val) => acc + val.travelDuration, 0) <=
						filter.transferDurationMax,
				)
			setSortedFlights(tempSortedFlights)
		}
	}, [filter])

	return <></>
}

export const Results: FC<{ flights: FlightsType }> = ({ flights }) => {
	const lang = useAppSelector(selectLang)
	const [isOpen, setIsOpen] = useState<boolean>(true)
	const [sortedFlights, setSortedFlights] = useState<FlightsType>(flights)
	const dispatch = useAppDispatch()
	// const applyFilterState = useAppSelector(getApplyFilter)
	const applyFilterState = false

	return (
		<>
			<div id="results">
				<p>
					<Link href="/">
						<a>{locales[lang].flights.search} -</a>
					</Link>
					{locales[lang].flights.results}
				</p>
				<div id="results__header">
					<p>{locales[lang].flights.results}</p>
					<div
						onClick={() => {
							setIsOpen(prev => !prev)
							dispatch(applyFilter(false))
						}}
					>
						{locales[lang].flights.filter.toLowerCase()}
					</div>
				</div>
				{sortedFlights[0] && (
					<div className="results__container">
						<p>{locales[lang].flights.optimal}</p>
						<FlightPreview flight={sortedFlights[0]} />
					</div>
				)}
				{applyFilterState && (
					<Flights flights={sortedFlights} setIsOpen={setIsOpen} />
				)}
			</div>
			<ApplyFilter setSortedFlights={setSortedFlights} flights={flights} />
			<DownModal
				title={locales[lang].flights.filter}
				close={() => {
					dispatch(applyFilter(true))
					setIsOpen(prev => !prev)
				}}
				isOpen={isOpen}
			>
				<Filter
					flights={flights}
					close={() => {
						dispatch(applyFilter(true))
						setIsOpen(prev => !prev)
					}}
				/>
			</DownModal>
		</>
	)
}

const Flights: FC<{
	flights: FlightsType
	setIsOpen: Dispatch<SetStateAction<boolean>>
}> = ({ flights: sortedFlights, setIsOpen }) => {
	const lang = useAppSelector(selectLang)

	return (
		<>
			{sortedFlights[1] && (
				<div className="results__container">
					<p>{locales[lang].flights.cheapest}</p>
					<FlightPreview flight={sortedFlights[1]} />
				</div>
			)}
			{sortedFlights[2] && (
				<div className="results__container results__container__other">
					<p>{locales[lang].flights.other}</p>
					{sortedFlights.slice(2).map(flight => (
						<FlightPreview flight={flight} key={flight.token} />
					))}
				</div>
			)}
		</>
	)
}
