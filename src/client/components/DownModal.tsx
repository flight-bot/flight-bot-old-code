import { FC, ReactNode, useEffect, useState } from 'react'
import Close from '../assets/close.svg'

type props = {
	title: string
	children?: ReactNode
	close: () => void
	isOpen: boolean
}

export const DownModal: FC<props> = ({ title, children, close, isOpen }) => {
	const [isFirst, setIsFirst] = useState<boolean>(true)

	useEffect(() => {
		if (isOpen) {
			setIsFirst(false)
		}
	}, [isOpen])

	return (
		<>
			<div
				className={`down-modal ${
					isOpen
						? 'down-modal-open'
						: isFirst
						? 'down-modal-init'
						: 'down-modal-close'
				}`}
			>
				<div className="down-modal__header">
					<p>{title}</p>
					<Close onClick={close} />
				</div>
				<div className="down-modal__content">{children}</div>
			</div>
			<div
				id="down-modal-blur"
				onClick={close}
				style={isOpen ? { opacity: 1 } : { opacity: 0, visibility: 'hidden' }}
			/>
		</>
	)
}
