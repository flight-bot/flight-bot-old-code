import { FC, useState } from 'react'
import { useAppDispatch, useAppSelector } from '../hooks/redux'
import { getWho, setWho } from '../redux/reducers/searchSlice'
import Plus from '../assets/plus.svg'
import Minus from '../assets/minus.svg'
import { selectLang } from '../redux/reducers/langSlice'
import locales from '../locales.json'
import { DeepMergeTwoTypes } from '../../shared/types/language'

type props = {
	title: string
	age: string
	key: string
}[]

export const PeopleSelect: FC<{ values: props; close: () => void }> = ({
	values,
	close,
}) => {
	const dispatch = useAppDispatch()
	const people = useAppSelector(getWho)
	const lang = useAppSelector(selectLang)
	const [block, setBlock] = useState<
		DeepMergeTwoTypes<typeof people, { all: number }>
	>({
		all: 0,
		ADULT: 0,
		CHILD: 0,
		INFANT: 0,
		INFANT_WITH_SITE: 0,
	})
	const [error, setError] = useState<string>('')

	function handleClick({
		key,
		operation,
	}: {
		key: string
		operation: 'plus' | 'minus'
	}) {
		setError('')
		if (people[key] + 1 >= 5 && operation == 'plus') {
			setError(locales[lang].errors.five_passengers)
			setBlock(prev => {
				return { ...prev, [key]: 1 }
			})
		} else {
			if (block[key])
				setBlock(prev => {
					return { ...prev, [key]: 0 }
				})
		}
		if (
			Object.values(people).reduce((acc, curr) => acc + curr, 0) >= 8 &&
			operation == 'plus'
		) {
			setError(locales[lang].errors.max)
			setBlock(prev => {
				return { ...prev, all: 1 }
			})
		} else {
			if (block.all)
				setBlock(prev => {
					return { ...prev, all: 0 }
				})
		}
		if (
			people['INFANT'] + 1 >= people['ADULT'] &&
			(key == 'INFANT' || (key == 'ADULT' && operation == 'minus'))
		) {
			setError(locales[lang].errors.baby)
			setBlock(prev => {
				return { ...prev, babies: 1 }
			})
		} else {
			if (block.INFANT && operation != 'minus' && key == 'ADULT')
				setBlock(prev => {
					return { ...prev, babies: 0 }
				})
		}
		dispatch(setWho({ key, operation }))
	}

	return (
		<div id="people-select">
			{values.map(({ title, age, key }) => (
				<div className="people-select__value" key={key}>
					<div className="people-select__value__text">
						<p>{title}</p>
						<p>{age}</p>
					</div>
					<div className="people-select__value__size">
						<div onClick={() => handleClick({ key, operation: 'minus' })}>
							<Minus />
						</div>
						<p>{people[key]}</p>
						<div
							onClick={() => handleClick({ key, operation: 'plus' })}
							className={
								block.all || block[key]
									? 'people-select__value__size__block'
									: ''
							}
						>
							<Plus />
						</div>
					</div>
				</div>
			))}
			<p id="people-select__error">{error}</p>
			<button onClick={close}>{locales[lang].index.select}</button>
		</div>
	)
}
