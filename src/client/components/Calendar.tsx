import { FC } from 'react'
import { useAppDispatch, useAppSelector } from '../hooks/redux'
import { selectLang } from '../redux/reducers/langSlice'
import {
	getDate,
	getDateBack,
	setDate,
	setDateBack,
} from '../redux/reducers/searchSlice'
import locales from '../locales.json'

const months = [
	{ ru: 'Январь', en: 'January' },
	{ ru: 'Февраль', en: 'February' },
	{
		ru: 'Март',
		en: 'March',
	},
	{ ru: 'Апрель', en: 'April' },
	{ ru: 'Май', en: 'May' },
	{ ru: 'Июнь', en: 'June' },
	{
		ru: 'Июль',
		en: 'July',
	},
	{ ru: 'Август', en: 'August' },
	{ ru: 'Сентябрь', en: 'September' },
	{
		ru: 'Октябрь',
		en: 'October',
	},
	{ ru: 'Ноябрь', en: 'November' },
	{ ru: 'Декабрь', en: 'December' },
]

const weeks = [
	{ ru: 'пн', en: 'mon' },
	{ ru: 'вт', en: 'tue' },
	{ ru: 'ср', en: 'wed' },
	{
		ru: 'чт',
		en: 'thu',
	},
	{ ru: 'пт', en: 'fri' },
	{ ru: 'сб', en: 'sat' },
	{ ru: 'вс', en: 'sun' },
]

export const Calendar: FC<{ close: () => void; isBack?: boolean }> = ({
	close,
	isBack,
}) => {
	const dates = getArrayDate()
	const dispatch = useAppDispatch()
	const date = new Date(useAppSelector(!isBack ? getDate : getDateBack))
	const lang = useAppSelector(selectLang)

	function checkDate(currDate: Date) {
		return (
			currDate.getFullYear() == date.getFullYear() &&
			currDate.getMonth() == date.getMonth() &&
			currDate.getDate() == date.getDate()
		)
	}

	return (
		<div id="calendar">
			{dates.map(
				({ month, values, daysAfterLastWeek, daysBeforeFirstWeek }) => (
					<div className="calendar__container" key={month}>
						<p className="calendar__container__month">
							{months.concat(months)[month][lang]}
						</p>
						<div className="calendar__container__week">
							{weeks.map(w => (
								<p key={w[lang]}>{w[lang]}</p>
							))}
						</div>
						<div className="calendar__container__grid">
							{values.map((v, i) => {
								const isDisabled =
									i < daysBeforeFirstWeek
										? true
										: i >= values.length - daysAfterLastWeek
								return (
									<p
										key={`${month}-${v}-${i}`}
										className={
											isDisabled ? 'calendar__container__grid__disable' : ''
										}
										id={
											checkDate(new Date(new Date().getFullYear(), month, v)) &&
											!isDisabled
												? 'calendar__container__grid__selected'
												: ''
										}
										onClick={() =>
											isDisabled
												? false
												: dispatch(
														!isBack
															? setDate(
																	new Date(
																		new Date().getFullYear(),
																		month,
																		v,
																	).getTime(),
															  )
															: setDateBack(
																	new Date(
																		new Date().getFullYear(),
																		month,
																		v,
																	).getTime(),
															  ),
												  )
										}
									>
										{v}
									</p>
								)
							})}
						</div>
					</div>
				),
			)}
			<button
				onClick={close}
				id={'calendar__button'}
				className={
					useAppSelector(!isBack ? getDate : getDateBack)
						? 'calendar__button-visible'
						: 'calendar__button-hidden'
				}
			>
				{locales[lang].index.select}
			</button>
		</div>
	)
}

type ArrayType = {
	values: number[]
	month: number
	daysAfterLastWeek: number
	daysBeforeFirstWeek: number
}[]

function getArrayDate(): ArrayType {
	const year = new Date().getFullYear()
	const month = new Date().getMonth()
	const array: ArrayType = []
	for (let i = 0; i < 12; i++) {
		const prevMonth = new Date(year, month + i, 0).getDate()
		const currentMonth = new Date(year, month + i + 1, 0).getDate()
		const nextMonthFirstDay = new Date(year, month + i + 1, 1).getDay()
		const firstDayOfMonth = new Date(year, month + i, 1).getDay()
		const daysBeforeFirstWeek = 6 - ((7 - firstDayOfMonth) % 7)
		const daysAfterLastWeek =
			7 - nextMonthFirstDay + 1 > 6 ? 0 : 7 - nextMonthFirstDay + 1
		const result = []
		for (let d = daysBeforeFirstWeek; d > 0; d--) {
			result.push(prevMonth - d + 1)
		}
		for (let d = 1; d <= currentMonth; d++) {
			result.push(d)
		}
		for (let d = 1; d <= daysAfterLastWeek; d++) {
			result.push(d)
		}
		array.push({
			month: month + i,
			values: result,
			daysBeforeFirstWeek,
			daysAfterLastWeek,
		})
	}
	return array
}
