import { useCallback, useState } from 'react'

export const useRequest = (): {
	request: (
		url: string,
		method?: 'POST' | 'GET' | 'PUT' | 'DELETE',
		body?: string | FormData,
		headers?: HeadersInit,
	) => Promise<any>
	loaded: boolean
} => {
	const [loaded, setLoaded] = useState<boolean>(true)
	const request = useCallback(
		async (
			url: string,
			method: string = 'GET',
			body: string | null | FormData = null,
			headers: HeadersInit = {},
		): Promise<any> => {
			setLoaded(false)
			const response = await fetch(url, {
				method,
				body,
				headers:
					typeof body == 'string'
						? { ...headers, 'Content-Type': 'application/json' }
						: headers,
			})
			const data = await response.json()
			setLoaded(true)
			return data
		},
		[],
	)
	return { request, loaded }
}
