import {
	configureStore,
	ThunkAction,
	Action,
	combineReducers,
} from '@reduxjs/toolkit'
import { createWrapper } from 'next-redux-wrapper'
import {
	persistReducer,
	FLUSH,
	REHYDRATE,
	PAUSE,
	PERSIST,
	PURGE,
	REGISTER,
} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import langSlice from './reducers/langSlice'
import searchSlice from './reducers/searchSlice'
import filterSlice from './reducers/filterSlice'

type StateStore = {
	language: ReturnType<typeof langSlice>
	search: ReturnType<typeof searchSlice>
	filter: ReturnType<typeof filterSlice>
}

const persistConfig = {
	key: 'root',
	storage,
}

const rootReducer = combineReducers({
	language: langSlice,
	search: searchSlice,
	filter: filterSlice,
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

const makeStore = () =>
	configureStore<StateStore>({
		reducer: persistedReducer,
		//@ts-ignore
		middleware: getDefaultMiddleware =>
			getDefaultMiddleware({
				serializableCheck: {
					ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
				},
			}),
		devTools: true,
	})

export type AppStore = ReturnType<typeof makeStore>
export type AppState = ReturnType<AppStore['getState']>
export type AppDispatch = AppStore['dispatch']
export type AppThunk<ReturnType = void> = ThunkAction<
	ReturnType,
	AppState,
	unknown,
	Action
>

//@ts-ignore
export const wrapper = createWrapper<AppStore>(makeStore)
