import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import type { AppState } from '../store'
import { PORT } from '../../../shared/constants'

type language = 'ru' | 'en'

interface LangState {
	lang: language
}

const initialState: LangState = {
	lang: 'ru',
}

export const langSlice = createSlice({
	name: 'lang',
	initialState,
	reducers: {
		changeLanguage: state => {
			window.fetch(`/api/language`, {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				credentials: 'include',
				body: JSON.stringify({ lang: state.lang == 'ru' ? 'en' : 'ru' }),
			})
			state.lang = state.lang == 'ru' ? 'en' : 'ru'
		},
		setLanguage: (state, action: PayloadAction<language>) => {
			state.lang = action.payload
		},
	},
})

export const { changeLanguage, setLanguage } = langSlice.actions

export const selectLang = (state: AppState) => state.language.lang

export default langSlice.reducer
