import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import type { AppState } from '../store'

interface FilterState {
	minPrice?: number
	maxPrice?: number
	departureMin?: number
	departureMax?: number
	arrivalMin?: number
	arrivalMax?: number
	transfer?: number[]
	transferDurationMin?: number
	transferDurationMax?: number
	roadMin?: number
	roadMax?: number
	airlines?: string[]
	applyFilter: boolean
}

const initialState: FilterState = {
	applyFilter: true,
}

export const filterSlice = createSlice({
	name: 'filter',
	initialState,
	reducers: {
		setMinPrice: (state, action: PayloadAction<number>) => {
			state.minPrice = action.payload
		},
		setMaxPrice: (state, action: PayloadAction<number>) => {
			state.maxPrice = action.payload
		},
		setMinDeparture: (state, action: PayloadAction<number>) => {
			state.departureMin = Number(action.payload)
		},
		setMaxDeparture: (state, action: PayloadAction<number>) => {
			state.departureMax = Number(action.payload)
		},
		setMinArrival: (state, action: PayloadAction<number>) => {
			state.arrivalMin = Number(action.payload)
		},
		setMaxArrival: (state, action: PayloadAction<number>) => {
			state.arrivalMax = Number(action.payload)
		},
		setTransfer: (state, action: PayloadAction<number>) => {
			if (!state.transfer) state.transfer = []
			const index = state.transfer.indexOf(action.payload)
			if (index == -1) state.transfer.push(action.payload)
			else state.transfer.splice(index, 1)
		},
		setTransferDurationMin: (state, action: PayloadAction<number>) => {
			state.transferDurationMin = Number(action.payload)
		},
		setTransferDurationMax: (state, action: PayloadAction<number>) => {
			state.transferDurationMax = Number(action.payload)
		},
		setRoadMin: (state, action: PayloadAction<number>) => {
			state.roadMin = Number(action.payload)
		},
		setRoadMax: (state, action: PayloadAction<number>) => {
			state.roadMax = Number(action.payload)
		},
		setAirlines: (state, action: PayloadAction<string>) => {
			if (!state.airlines) state.airlines = []
			const index = state.airlines.indexOf(action.payload)
			if (index == -1) state.airlines.push(action.payload)
			else state.airlines.splice(index, 1)
		},
		applyFilter: (state, action: PayloadAction<boolean>) => {
			state.applyFilter = action.payload
		},
	},
})

export const {
	setMinPrice,
	setMaxPrice,
	setMinDeparture,
	setMaxDeparture,
	setMinArrival,
	setMaxArrival,
	setTransfer,
	setTransferDurationMin,
	setTransferDurationMax,
	setRoadMin,
	setRoadMax,
	setAirlines,
	applyFilter,
} = filterSlice.actions

export const getFilter = (state: AppState) => state.filter
export const getApplyFilter = (state: AppState) => state.filter.applyFilter

export default filterSlice.reducer
