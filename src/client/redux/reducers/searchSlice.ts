import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import type { AppState } from '../store'

export interface SearchState {
	isOneDirection: boolean
	to: string
	from: string
	codeTo?: string
	codeFrom?: string
	date?: number
	who: {
		ADULT: number
		CHILD: number
		INFANT: number
		INFANT_WITH_SITE: number
	}
	class: string
	additional: boolean
	direct: boolean
	dateBack?: number
}

const initialState: SearchState = {
	date: undefined,
	isOneDirection: true,
	to: '',
	from: '',
	who: { ADULT: 1, CHILD: 0, INFANT: 0, INFANT_WITH_SITE: 0 },
	class: '',
	additional: false,
	direct: false,
}

export const SearchSlice = createSlice({
	name: 'search',
	initialState,
	reducers: {
		setIsOneDirection: (state, action: PayloadAction<boolean>) => {
			state.isOneDirection = action.payload
		},
		to: (state, action: PayloadAction<string>) => {
			if (!action.payload) state.codeTo = undefined
			state.to = action.payload
		},
		from: (state, action: PayloadAction<string>) => {
			if (!action.payload) state.codeFrom = undefined
			state.from = action.payload
		},
		changeAdditional: state => {
			state.additional = !state.additional
		},
		changeDirect: state => {
			state.direct = !state.direct
		},
		setWho: (
			state,
			action: PayloadAction<{ key: string; operation: 'plus' | 'minus' }>,
		) => {
			if (action.payload.operation == 'plus') {
				if (Object.values(state.who).reduce((acc, curr) => acc + curr, 0) > 8)
					return
				if (
					action.payload.key == 'babies' &&
					state.who['babies'] >= state.who['adults']
				)
					return
				if (state.who[action.payload.key] < 5)
					state.who[action.payload.key] += 1
			}
			if (
				action.payload.operation == 'minus' &&
				state.who[action.payload.key] > 0
			) {
				if (
					action.payload.key == 'adults' &&
					state.who['babies'] >= state.who['adults']
				) {
					state.who['babies'] -= 1
				}
				state.who[action.payload.key] -= 1
			}
		},
		setClass: (state, action: PayloadAction<string>) => {
			state.class = action.payload
		},
		setDate: (state, action: PayloadAction<number>) => {
			state.date = action.payload
		},
		setDateBack: (state, action: PayloadAction<number>) => {
			state.dateBack = action.payload
		},
		setCodeTo: (state, action: PayloadAction<string>) => {
			state.codeTo = action.payload
		},
		setCodeFrom: (state, action: PayloadAction<string>) => {
			state.codeFrom = action.payload
		},
	},
})

export const {
	setIsOneDirection,
	to,
	from,
	changeDirect,
	changeAdditional,
	setWho,
	setClass,
	setDate,
	setDateBack,
	setCodeTo,
	setCodeFrom,
} = SearchSlice.actions

export const getSearchData = (state: AppState) => state.search
export const getIsOneDirection = (state: AppState) =>
	state.search.isOneDirection
export const getWho = (state: AppState) => state.search.who
export const getDate = (state: AppState) => state.search.date
export const getDateBack = (state: AppState) => state.search.dateBack

export default SearchSlice.reducer
